package com.akshay.recyclerviewdemo;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by akshay on 17/2/15.
 */
public class MyAdapter extends RecyclerView.Adapter<NameHolder> {

    List<Names> mList;
    Context context;

    public MyAdapter(List<Names> mList, Context context) {
        this.mList = mList;
        this.context = context;
    }

    @Override
    public NameHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.row_name, parent, false);
        NameHolder holder = new NameHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(NameHolder holder, int position) {
        if (holder != null) {
            Names names = mList.get(position);
            holder.lblName.setText(mList.get(position).getFirstName());
            holder.lblSurname.setText(mList.get(position).getLastName());
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}
