package com.akshay.recyclerviewdemo;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by akshay on 17/2/15.
 */
public class NameHolder extends RecyclerView.ViewHolder {

    TextView lblName, lblSurname;

    public NameHolder(View view) {
        super(view);
        lblName = (TextView) view.findViewById(R.id.lbl_name);
        lblSurname = (TextView) view.findViewById(R.id.lbl_surname);

    }
}
